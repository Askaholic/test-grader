extern crate regex;
extern crate threadpool;
extern crate zip;
#[macro_use] extern crate lazy_static;


use std::path::{Path, PathBuf};
use std::process::Command;
use std::sync::{Arc, Mutex};
use std::ffi::OsStr;
use std::fs::File;
use std::io::Read;

use regex::{Regex, Captures};
use threadpool::{ThreadPool};


mod unzip;
mod util;

pub use unzip::unzip_archive;

lazy_static! {
    static ref CURRENT_UID: Arc<Mutex<usize>> = Arc::new(Mutex::new(0));
}

#[derive(Clone)]
pub struct Assignment {
    container_name: String,
    test_file: PathBuf,
    test_file_rename: Option<PathBuf>,
    tmp_dir: PathBuf
}

impl Assignment {
    pub fn new(container_name: String, test_file: PathBuf, tmp_dir: PathBuf) -> Assignment {
        Assignment {
            container_name,
            test_file,
            tmp_dir,
            test_file_rename: None
        }
    }

    pub fn rename_test_file(mut self, test_file: PathBuf) -> Assignment {
        self.test_file_rename = Some(test_file);
        self
    }

    /// Creates a temp dir which is automatically removed when this goes out of scope
    fn new_tmp_dir(&self) -> Result<util::TempDir, String> {
        let tmp_dir = self.tmp_dir.join(Path::new(&get_uid().to_string()));
        util::TempDir::new(tmp_dir)
    }

    fn get_test_file_path(&self) -> Result<PathBuf, String> {
        self.test_file.canonicalize().or::<String>(Err("Bad test file path".into()))
    }

    /// #Panics
    /// If test file path is a directory
    pub fn grade(&self, source_zip: PathBuf) -> Result<Score, String> {
        let test_file_path = self.get_test_file_path()?;
        let  mut test_file_dest = test_file_path.clone();
        if let Some(file) = &self.test_file_rename {
            test_file_dest.set_file_name(&file);
        }

        let unzip_dir = self.new_tmp_dir()?;

        unzip::unzip_archive(&source_zip, &unzip_dir)
            .or_else(|e| Err(format!("Failed to unzip source file: {}", e)))?;

        // Copy the test file into the temp directory
        std::fs::copy(
            &test_file_path,
            &unzip_dir.join(&test_file_dest.file_name().unwrap())
        ).or_else(|e| Err(format!("Failed to copy test file: {}", e)))?;

        self.grade_dir(&unzip_dir)
    }

    /// Runs the docker job against a given directory and reports the results
    pub fn grade_dir(&self, dir: &Path) -> Result<Score, String> {
        let _timer = util::Timer::new(format!("Grading dir {:?}", dir));
        // Run the docker job
        let output = match Command::new("docker")
                                   .args(&["run", "-t", "-v",
                                         &format!("{}:/workdir/source:ro", dir.to_string_lossy()),
                                         &self.container_name])
                                   .output()  {
            Ok(o) => o,
            Err(e) => return Err(format!("Failed to run docker job: {}", e))
        };

        Ok(Score {
            tests: 0,
            passed: 0,
            output: String::from_utf8_lossy(&output.stdout).into_owned()
        })
    }

    pub fn grade_blackboard_bundle(&self, source_zip: PathBuf) -> Result<Vec<Result<(String, Score), String>>, String> {
        let unzip_dir = self.new_tmp_dir()?;

        {
            let _timer = util::Timer::new("Unzipping bundle".into());
            unzip::unzip_archive(&source_zip, &unzip_dir)
                .or_else(|e| Err(format!("Failed to unzip blackboard bundle: {}", e)))?;
        }
        let _timer = util::Timer::new("Grading assignments".into());
        // If this fails, then the temp dir permissions are seriosly weird
        let paths = std::fs::read_dir(&*unzip_dir)
            .or::<String>(Err("Failed to read from the directory we just successfully wrote to!".into()))?;

        let txt_files = paths
            .filter(|path| path.is_ok())
            .map(|path| path.unwrap())
            .filter(|dir_entry| dir_entry.path().extension() == Some(OsStr::new("txt")))
            .collect::<Vec<std::fs::DirEntry>>();

        const MAX_THREADS: usize = 8;
        let num_threads = if txt_files.len() > MAX_THREADS { MAX_THREADS } else { txt_files.len() };
        let pool = ThreadPool::new(num_threads);
        let accumulated_scores = Arc::new(Mutex::new(Vec::new()));
        for dir_entry in txt_files {
            let accumulated_scores = accumulated_scores.clone();
            let assignment = (*self).clone();
            pool.execute(move || {
                let score = Assignment::grade_from_blackboard_txt(&assignment, &dir_entry.path());
                accumulated_scores.lock().unwrap().push(score);
            });
        }
        pool.join();
        let accumulated_scores = Arc::try_unwrap(accumulated_scores)
            .expect("Lock still has multiple owners. Impossible!");
        Ok(accumulated_scores.into_inner().unwrap())
    }

    fn grade_from_blackboard_txt(&self, path: &Path) -> Result<(String, Score), String> {
        let mut file = File::open(path)
            .or::<String>(Err("Could not open txt file".into()))?;

        let mut contents = String::new();
        file.read_to_string(&mut contents)
            .or::<String>(Err("Could not read txt file".into()))?;

        let unzip_dir = self.new_tmp_dir()?;

        let info = parse_blackboard_txt(&contents);
        let user_name = info.user_name.unwrap_or("Unknown User").into();

        let files = &info.files.ok_or(format!("{} didn't submit any files", user_name))?;
        for filename in files {
            let file_path = path.parent().unwrap_or(Path::new(".")).join(Path::new(&filename));
            if file_path.extension() == Some(OsStr::new("zip")) {
                unzip::unzip_archive(&file_path, &unzip_dir)
                    .or_else(|e| Err(format!("Failed to unzip source file: {}", e)))?;
            }
            else {
                lazy_static! {
                    static ref FILE_NAME: Regex = Regex::new(r"^.*?_[a-z0-9]+_attempt_\d{4}(?:-\d{2}){5}_(.*)$").unwrap();
                }
                let file_path_string = &file_path.to_string_lossy();
                let dst_file_name = capture_get(1, FILE_NAME.captures(&file_path_string))
                    .map_or(Path::new(file_path.file_name().unwrap()), |p| Path::new(p));
                std::fs::copy(
                    &file_path,
                    &unzip_dir.join(&dst_file_name)
                ).or_else(|e| Err(format!("Failed to copy source file {}: {}", file_path.to_string_lossy(), e)))?;
            }
        }
        let test_file_path = self.get_test_file_path()?;
        let test_file_dest = &unzip_dir.join(
            match self.test_file_rename {
                Some(ref file) => file.as_os_str(),
                None => test_file_path.file_name().unwrap()
            }
        );
        // Copy the test file into the temp directory
        std::fs::copy(&test_file_path, &test_file_dest)
            .or_else(|e| Err(format!("Failed to copy test file: {}", e)))?;

        let score = self.grade_dir(&unzip_dir)?;

        Ok((user_name, score))
    }
}

#[derive(Debug)]
pub struct Score {
    pub tests: usize,
    pub passed: usize,
    pub output: String
}


#[derive(Debug, PartialEq)]
struct BlackboardAssignmentInfo<'t> {
    full_name: Option<&'t str>,
    user_name: Option<&'t str>,
    assignment: Option<&'t str>,
    date_submitted: Option<&'t str>,
    current_grade: Option<&'t str>,
    submission: Option<&'t str>,
    comments: Option<&'t str>,
    files: Option<Vec<&'t str>>
}

fn get_uid() -> usize {
    let mut cur_id = CURRENT_UID.lock().unwrap();
    let uid = *cur_id;
    *cur_id += 1;

    uid
}


fn parse_blackboard_txt(text: &str) -> BlackboardAssignmentInfo {
    lazy_static! {
        static ref NAME: Regex = Regex::new(r"Name:\s((?:\w| )+) \(([a-z0-9]+)\)\n").unwrap();
        static ref ASSIGNMENT: Regex = Regex::new(r"Assignment:\s((?:\w| )+)\n").unwrap();
        static ref DATE: Regex = Regex::new(r"Date Submitted:\s(.+)\n").unwrap();
        static ref GRADE: Regex = Regex::new(r"Current Grade:\s((?:\w| )+)\n").unwrap();
        static ref SUBMISSION: Regex = Regex::new(r"Submission Field:\s((?:.|\n)*?)(?:\n\n|$)").unwrap();
        static ref COMMENTS: Regex = Regex::new(r"Comments:\s((?:.|\n)*?)(?:\n\n|$)").unwrap();
        static ref FILES: Regex = Regex::new(r"Files:\s((?:.|\n)*?)(?:$)").unwrap();
        static ref FILE: Regex = Regex::new(r"Filename:\s(.+?)(?:\n|$)").unwrap();
    }

    let files = match capture_get(1, FILES.captures(text)) {
        None => None,
        Some(content) => {
            let filenames = FILE.captures_iter(content)
                .filter_map(|c| capture_get(1, Some(c)))
                .collect::<Vec<&str>>();

            if filenames.len() == 0 { None } else { Some(filenames) }
        }
    };

    BlackboardAssignmentInfo {
        full_name: capture_get(1, NAME.captures(text)),
        user_name: capture_get(2, NAME.captures(text)),
        assignment: capture_get(1, ASSIGNMENT.captures(text)),
        date_submitted: capture_get(1, DATE.captures(text)),
        current_grade: capture_get(1, GRADE.captures(text)),
        submission: capture_get(1, SUBMISSION.captures(text)),
        comments: capture_get(1, COMMENTS.captures(text)),
        files: files
    }
}

fn capture_get<'t>(i: usize, captures: Option<Captures<'t>>) -> Option<&'t str> {
    Some(captures?.get(i)?.as_str())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_blackboard_assignment() {
        let info = parse_blackboard_txt(
            "Name: Rohan Weeden (reweeden)\n\
             Other garbage
             Assignment: Assignment 1\nOther garbage\n\
             Date Submitted: Thursday, January 31, 2019 9:41:35 PM AKST\n\
             Other garbage\n\
             Current Grade: Needs Grading\n\
             Submission Field:\n\
             There is no student submission text data for this assignment.\n\n\
             Other garbage\n\
             Comments:\n\
             There are no student comments for this assignment.\n\n\
             Files:\n\
	            Original filename: Homework 1.pdf\n\
                Filename: Assignment 1_reweeden_attempt_2019-01-31-12-00-00_Homework 1.pdf\n\
   	            Original filename: Homework 2.pdf\n\
                Filename: Assignment 1_reweeden_attempt_2019-01-31-12-00-00_Homework 2.pdf\n"
        );
        assert_eq!(BlackboardAssignmentInfo {
            full_name: Some("Rohan Weeden"),
            user_name: Some("reweeden"),
            assignment: Some("Assignment 1"),
            date_submitted: Some("Thursday, January 31, 2019 9:41:35 PM AKST"),
            current_grade: Some("Needs Grading"),
            submission: Some("There is no student submission text data for this assignment."),
            comments: Some("There are no student comments for this assignment."),
            files: Some(vec![
                "Assignment 1_reweeden_attempt_2019-01-31-12-00-00_Homework 1.pdf",
                "Assignment 1_reweeden_attempt_2019-01-31-12-00-00_Homework 2.pdf"
            ])
        }, info)
    }
}
