extern crate test_grader;

use std::path::Path;

fn main() {
    let assignment = test_grader::Assignment::new(
        "unittest:lua".into(),
        Path::new("../cs331_2019_01/pa2_test.lua").to_path_buf(),
        Path::new("/tmp/assignments").to_path_buf()
    ).rename_test_file(Path::new("test.lua").to_path_buf());

    let mut grades = assignment.grade_blackboard_bundle(
        Path::new("/home/dope/Downloads/gradebook_CSCE_A331_30761_201901_Assignment202_2019-02-13-17-40-22.zip").to_path_buf()
    ).unwrap();

    grades.sort_by_key(|g| {
        match g {
            Ok((user, _)) => user.clone(),
            Err(_) => "".into()
        }
    });

    for grade in grades {
        match grade {
            Ok((user, score)) => {
                println!("{} {}/{}\n{}\n", user, score.passed, score.tests, score.output);
            },
            Err(e) => println!("{}", e)
        }
    }
}
