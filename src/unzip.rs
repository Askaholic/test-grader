use std::fs::File;
use std::path::{Path, PathBuf};
use std::io::{Write, Read};

use threadpool::{ThreadPool};

static BUF_SIZE: usize = 4096;

/**
Reads a zip file from `src` and writes the decompressed contents into the directory `dst`.
Each file in the zip archive is decompressed in it's own thread.
# Examples

```rust,no_run
use std::path::Path;
use test_grader::unzip_archive;

// test/ will contain the contents of test.zip
unzip_archive(Path::new("test.zip"), Path::new("test/")).unwrap();
```

# Errors

Any IO errors will be propagated out. If any of the threads fail, it will return an IO error with
ErrorKind::Other.
*/
pub fn unzip_archive(src: &Path, dst: &Path) -> std::io::Result<()> {
    let file = File::open(src)?;
    let zip = zip::ZipArchive::new(file)?;

    let pool = ThreadPool::new(3);

    let mut handles = Vec::with_capacity(zip.len());
    for i in 0..zip.len() {
        let src = src.to_path_buf();
        let dst = dst.to_path_buf();
        handles.push(
            pool.execute(move || unzip_file_thread(src, i, dst).unwrap())
        );

    }

    pool.join();
    if pool.panic_count() > 0 {
        Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "Some threads failed to unzip")
        )
    }
    else {
        Ok(())
    }
}

/// # Panics
/// If index does not point to a valid file
fn unzip_file_thread(src: PathBuf, index: usize, dst: PathBuf) -> std::io::Result<()> {
    let file = File::open(src)?;
    let mut zip = zip::ZipArchive::new(file)?;
    let file = zip.by_index(index).unwrap();

    unzip_file(file, &dst)
}

fn unzip_file(mut file: zip::read::ZipFile, dst: &Path) -> std::io::Result<()> {
    let path = file.sanitized_name();
    let final_path = dst.join(path);

    // Create the parent directory if needed
    if let Some(path) = final_path.parent() {
        if !path.exists() {
            std::fs::create_dir_all(path)?;
        }
    }

    match file.size() {
        0 => std::fs::create_dir_all(final_path)?,
        _ => {
            let mut outfile = File::create(final_path)?;
            copy_file_buffered(&mut file, &mut outfile)?;
        }
    }

    Ok(())
}

fn copy_file_buffered(infile: &mut zip::read::ZipFile, outfile: &mut Write) -> std::io::Result<()> {
    let file_size = infile.size() as usize;
    let buf_size = if file_size < BUF_SIZE { file_size } else { BUF_SIZE };
    let mut buf = vec![0; buf_size];

    loop {
        let n = infile.read(buf.as_mut_slice())?;
        if n == 0 { break; }

        outfile.write(&buf[..n])?;
    }

    Ok(())
}
