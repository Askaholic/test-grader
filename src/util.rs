use std::time::Instant;
use std::path::PathBuf;
use std::ops::Deref;


pub struct Timer {
    message: String,
    start_time: Instant
}

impl Timer {
    pub fn new(message: String) -> Timer {
        let start_time = Instant::now();
        println!("{}", message);
        Timer {
            message,
            start_time
        }
    }
}

impl Drop for Timer {
    fn drop(&mut self) {
        println!("{} complete in {:?}", self.message, self.start_time.elapsed());
    }
}

/**
A wrapper around a PathBuf which is created when initialized and removed when dropped.
*/
pub struct TempDir(PathBuf);
impl TempDir {
    pub fn new(path: PathBuf) -> Result<TempDir, String> {
        std::fs::create_dir_all(&path)
            .or::<String>(Err("Failed to create temp dir".into()))?;

        Ok(TempDir(path))
    }
}
impl Drop for TempDir {
    fn drop(&mut self) {
        // If the remove fails, there's nothing we can do so we just ignore it
        std::fs::remove_dir_all(self.0.to_string_lossy().into_owned()).ok();
    }
}
impl Deref for TempDir {
    type Target = PathBuf;
    fn deref(&self) -> &PathBuf { &self.0 }
}
